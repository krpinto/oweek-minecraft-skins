#! /usr/local/bin/python3

import log
import json

from os import listdir as ls
from os import mkdir as mkdir
from os import chdir as cd
from os import remove as rm

from os.path import isfile
from os.path import isdir
from os.path import basename
from os.path import splitext

from shutil import rmtree as rmdir
from shutil import copy as cp
from shutil import copytree as cpdir
from shutil import make_archive as compress

from uuid import uuid4
from sys import argv

def skins_json(pack, skins):
	path = "%s/%s" % (STAGING_DIRECTORY, pack)

	# open JSON file
	file = open("%s/skins.json" % path, "r+")
	meta = json.load(file)

	# copy skins metadata
	entry = meta["skins"][0].copy()
	meta["skins"] = []

	# generate metadata for each skin
	for skin in skins:
		entry["localization_name"] = skin
		entry["texture"] = "%s.png" % skin

		if skin.lower().endswith(" (slim)"):
			log.debug("Using slim model for '%s'" % skin)
			entry["geometry"] = "geometry.humanoid.customSlim"
		else:
			log.debug("Using classic model for '%s'" % skin)
			entry["geometry"] = "geometry.humanoid.custom"

		meta["skins"].append(entry.copy())

	# generate pack metadata
	meta["serialize_name"] = pack
	meta["localization_name"] = pack

	# dump metadata back to JSON file
	file.seek(0)
	file.truncate(0)
	file.write(json.dumps(meta, indent = 4))
	file.close()

def manifest_json(pack):
	pack_path = "%s/%s" % (STAGING_DIRECTORY, pack)

	# open JSON file
	file = open("%s/manifest.json" % pack_path, "r+")
	meta = json.load(file)

	# generate pack metadata
	meta["header"]["name"] = pack
	meta["header"]["uuid"] = str(uuid4())
	meta["modules"][0]["uuid"] = str(uuid4())

	# dump metadata back to JSON file
	file.seek(0)
	file.truncate(0)
	file.write(json.dumps(meta, indent = 4))
	file.close()

def texts_lang(pack, skins):
	pack_path = "%s/%s" % (STAGING_DIRECTORY, pack)

	# open lang file
	file = open("%s/texts/en_US.lang" % pack_path, "r+")

	# write skin metadata to the file
	file.seek(0)
	file.truncate(0)
	file.write("skinpack.%s=%s\r\n" % (pack, pack))
	for skin in skins:
		file.write("skin.%s.%s=%s\r\n" % (pack, skin, skin))
	file.close()

def generate_skinpack(pack, skins):
	log.debug("Generating metadata for '%s' ....." % pack)
	skins_json(pack, skins)
	manifest_json(pack)
	texts_lang(pack, skins)

def discover_skins(directory):
	skin_packs = ls(directory)

	if len(skin_packs) < 1:
		log.error("No skinpack directories found under '%s'" % directory)
		exit(-1)

	registry = {}

	for pack_name in skin_packs:

		if pack_name.startswith("."):
			continue   # skip hidden files

		pack_path = directory
		pack_path += "/%s" % pack_name

		if not isdir(pack_path):
			log.error("'%s' is not a directory" % pack_path)
			exit(-1)

		skin_files = ls(pack_path)
		skin_names = []

		if len(skin_files) < 1:
			log.error("No skins found under '%s'" % pack_path)
			exit(-1)

		for skin_file in skin_files:
			skin_path = pack_path
			skin_path += "/%s" % skin_file

			if not isfile(skin_path):
				log.error("'%s' is not a file" % skin_path)
				exit(-1)

			if skin_file.lower().startswith("."):
				log.warning("Ignoring '%s'" % skin_path)
				continue   # ignore hidden files

			if not skin_file.lower().endswith((".png")):
				log.error("The skin file '%s' is missing a '.png' extension" % skin_path)
				exit(-1)

			skin_names.append(splitext(skin_file)[0])
			log.debug("Found '%s'" % skin_path)

		registry[pack_name] = skin_names

	return registry

# ======================================================================= #

log.suppress("DEBUG")
log.colourless()

if len(argv) > 1:
	for i in range(1, len(argv)):
		if argv[i] == "--debug":
			log.show("DEBUG")
			log.trace(caller = True, file = False, line = False)
		elif argv[i] == "--colour":
			log.colourize()
		else:
			log.warning("Unknown option '%s'" % argv[i])

TEMPLATE_DIRECTORY = "template"
SKINS_DIRECTORY = "skins"
SKINPACKS_DIRECTORY = "skinpacks"
STAGING_DIRECTORY = "staging"

if not isdir(TEMPLATE_DIRECTORY):
	log.error("Could not find '%s' directory" % TEMPLATE_DIRECTORY)
	exit(-1)

if not isdir(SKINS_DIRECTORY):
	log.error("Could not find '%s' directory" % SKINS_DIRECTORY)
	exit(-1)

if not isdir(SKINPACKS_DIRECTORY):
	log.error("Could not find '%s' directory" % SKINPACKS_DIRECTORY)
	exit(-1)

log.info("Discovering skins .....")
registry = discover_skins(SKINS_DIRECTORY)

log.info("Setting up temporary directory .....")
if isdir(STAGING_DIRECTORY):
	rmdir(STAGING_DIRECTORY)
mkdir(STAGING_DIRECTORY)

log.info("Removing old skinpacks .....")
for pack_file in ls(SKINPACKS_DIRECTORY):
	if splitext(pack_file)[1] == ".mcpack":
		path = "%s/%s" % (SKINPACKS_DIRECTORY, pack_file)
		rm(path)
		log.debug("Deleting '%s'" % path)

for pack_name in registry.keys():
	pack_path = SKINS_DIRECTORY
	pack_path += "/%s" % pack_name

	# copy template skin pack
	log.debug("Copying skin pack template .....")
	staging_path = "%s/%s" % (STAGING_DIRECTORY, pack_name)
	cpdir(TEMPLATE_DIRECTORY, staging_path)

	# copy skins to template skin pack
	for skin_name in registry[pack_name]:
		skin_path = pack_path
		skin_path += "/%s.png" % skin_name
		log.debug("Copying skin '%s' from skinpack '%s'....." % (skin_name, pack_name))
		cp(skin_path, staging_path)

	log.debug("Copying skin pack template .....")
	# generate the metadata for the skin pack
	generate_skinpack(pack_name, registry[pack_name])

	# compress the skin pack into a .mcpack file
	log.debug("Compressing '%s' ....." % pack_name)
	compress(staging_path, "zip", staging_path)

	# remove the staging folder
	rmdir(staging_path)

	# copy the skin pack to the artifacts folder
	zip_path = "%s.zip" % staging_path
	mcpack_path = "%s/%s.mcpack" % (SKINPACKS_DIRECTORY, pack_name)
	cp(zip_path, mcpack_path)

	log.success("Created skin pack '%s'" % pack_name)

rmdir(STAGING_DIRECTORY)
